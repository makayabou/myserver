# Myserver

## Introduction

Ce projet permet de déployer un serveur Linux. On y trouve des instructions, des fichiers de configuration et des scripts.

## Instructions

Clonez le projet dans votre dossier personnel: 

```
git clone https://gitlab.com/makayabou/myserver.git
```

Entrez maintenant dans le dossier téléchargé:  
     cd myserver

Vous pouvez maintenant:  
- installer un serveur web  
- installer un serveur ssh  


